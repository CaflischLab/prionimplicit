# Information for: Antibody binding modulates the dynamics of the membrane-bound prion protein

By: I.M. ILIE, M. BACCI, A. VITALIS AND A.CAFLISCH
Year: 2022
DOI: XXX

The folder contains the initial conformations used in all simulations.
The names of the folders correspond to the nomenclature used in the paper.

Description of the input files:
The default naming needs to be adjusted to match the specifications in the key files
1. FRZFILE: 
	- contains the atoms required to describe the frozen dihedral angles
	- frozen are the dihedrals of the residues part of a well defined secondary structure (i.e. the atoms in the loops are not considered)
	- the starting residue is Tyr127 (i.e. the tail is fully flexible)
	- the numbering of the residues is shifted by 22 (hence the numbering starts at 1 and not at 23)
	- see http://campari.sourceforge.net/V4/keywords.html#DRESTFILE for the complete description of the keyword

2. DRESTFILE
	- restrains the distance between the C-atom in Ser231 and the membrane by adding a harmonic potential
	- see http://campari.sourceforge.net/V4/inputfiles.html#FMCSC_DRESTFILE for the complete description of the keyword

3. FOSPATCHFILE
	- adjusts the solvation free energy to override the default value (required to avoid dissociation of the complexes at high temperatures)
	- used only in the complexed systems
	- see http://campari.sourceforge.net/V4/inputfiles.html#FMCSC_FOSPATCHFILE for the complete description of the keyword
